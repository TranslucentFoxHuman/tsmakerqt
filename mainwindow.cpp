/*
 * mainwindow.cpp
 * Copyright (C) 2022 半透明狐人間
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "aboutdialog.h"
#include <QString>
#include <QClipboard>



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

int bytec = 3;
QClipboard *clipboard = QGuiApplication::clipboard();

std::string createts(std::string basetext,int lrspsize,int udspsize) {
    std::string fullstring = "＿";
    int len = int(basetext.length()) / bytec + (lrspsize*2);
    //std::cout << std::to_string(len) << "\n";
    for (int i = 1;i <= len;i++) {
        fullstring = fullstring + "人";
    }
    fullstring = fullstring + "＿\n";
    std::string udspace;
    {
        udspace = "＜";
        for (int i = 1;i <= (len);i++){
            udspace = udspace + "　";
        }
        udspace = udspace + "＞\n";
    }
    for (int i = 1;i <= udspsize;i++) {
        fullstring = fullstring + udspace;
    }
    fullstring = fullstring + "＜";
    for (int i = 1;i <= lrspsize;i++){
        fullstring = fullstring + "　";
    }
    fullstring = fullstring + basetext;
    for (int i = 1;i <= lrspsize;i++){
        fullstring = fullstring + "　";
    }
    fullstring = fullstring + "＞\n";
    for (int i = 1;i <= udspsize;i++) {
        fullstring = fullstring + udspace;
    }
    fullstring = fullstring + "￣";
    for (int i = 1;i <= len;i++) {
        fullstring = fullstring + "Y^";
    }
    fullstring = fullstring + "￣";
    return fullstring;
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    std::string inputtext = ui->lineEdit->text().toStdString();
    std::string createdtext = createts(inputtext,ui->LRSpSpinBox->value(),ui->UDSpSpinBox->value());
    ui->plainTextEdit->setPlainText(QString::fromStdString(createdtext));

}

void MainWindow::on_lineEdit_returnPressed()
{
    MainWindow::on_pushButton_clicked();
}

void MainWindow::on_CopyButton_clicked()
{
    clipboard->setText(ui->plainTextEdit->toPlainText());
}

void MainWindow::on_aboutAction_triggered() {
    AboutDialog *abtdiag = new AboutDialog(this);
    abtdiag->show();
}

